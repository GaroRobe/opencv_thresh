// opencv_thresh.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "opencv2/opencv.hpp"

int main()
{
	cv::VideoCapture cap(0);
	if (!cap.isOpened())
		return -1;

	cv::Mat frame, mask, gray, otsu;
	std::vector<std::vector<cv::Point>> contours;

	for (;;)
	{
		cap >> frame;
		imshow("source", frame);
		
		cv::inRange(frame, cv::Scalar{40, 40, 140}, cv::Scalar{160, 160, 255}, mask);
		imshow("mask", mask);

		cv::Mat color_thresh;
		cv::bitwise_and(frame, frame, color_thresh, mask);
		imshow("color_thresh", color_thresh);

		cvtColor(color_thresh, gray, cv::COLOR_BGR2GRAY);
		cv::threshold(gray, otsu, 100, 255, cv::THRESH_OTSU);
		cv::imshow("otsu", otsu);

		cv::findContours(otsu, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

		auto max_contour = std::max_element(contours.cbegin(), contours.cend(),
			[](const std::vector<cv::Point>& c1, const std::vector<cv::Point>& c2) {
			return contourArea(c1) < contourArea(c2);
		});
		cv::Mat max_frame(frame);
		if (max_contour != contours.cend())
		{
			cv::polylines(max_frame, *max_contour, true, cv::Scalar{ 0, 255, 0 }, 3);
			cv::imshow("max", max_frame);
		}

		if (cv::waitKey(30) >= 0)
			break;
	}

    return 0;
}

